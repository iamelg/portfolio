export class Date {
    constructor(day, month, year, holiday = false){
        this.day = day;
        this.month = month;
        this.year = year;
        this.holiday = holiday;
    }
}